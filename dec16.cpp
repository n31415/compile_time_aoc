#include "include/advent.hpp"
#include "include/algorithms.hpp"
#include "inputs/dec16.hpp"
#include <bitset>
#include <cstdint>
#include <iterator>
#include <type_traits>

using store_t = uint16_t;

constexpr auto hex_base = 16UL;
constexpr auto VTSize = 3UL;
constexpr auto HeaderBitSize = 2 * VTSize;
constexpr auto ValueSubPacketSize = 4UL;
constexpr auto BitLengthSize = 15UL;
constexpr auto CountLengthSize = 11UL;
constexpr auto BitsPerHex = 4UL;
constexpr auto InputBitSize = 2 * BitsPerHex * input.size();

using content_t = std::array<bool, InputBitSize>;

constexpr auto read_input(std::string_view in) -> content_t {
    content_t ret;
    remove_ws(in);
    auto i = size_t{};

    for (auto c = char{}; (c = get_char(in)) != 0; i += BitsPerHex) {
        auto digit = static_cast<uint8_t>(digit_to_number(c, hex_base));
        if (digit + 1 == 0) {
            break;
        }
        for (auto sub_i: std::views::iota(0UL, BitsPerHex)) {
            auto bit = static_cast<uint8_t>(digit >> sub_i) & 1UL;
            ret[i + BitsPerHex - 1 - sub_i] = (bit != 0);
        }
    }

    return ret;
}

constexpr auto content = read_input(input);

template<size_t N>
constexpr auto read_n(size_t &i) -> store_t {
    store_t ret{};
    for (auto sub_i: std::views::iota(i, i + N)) {
        ret <<= 1UL;
        ret += content.at(sub_i);
    }
    i += N;
    return ret;
}

enum class TypeId : uint8_t {
    Sum = 0,
    Product = 1,
    Minimum = 2,
    Maximum = 3,
    Value = 4,
    Greater = 5,
    Less = 6,
    Equal = 7,
};

struct Header {
    uint8_t version : 3, type : 3, lid : 1, /*padding*/ : 1;
};

static_assert(sizeof(Header) == 1);

struct Packet;
constexpr auto eval(TypeId type_id, const Packet & /*val*/) -> size_t;

struct Packet {
    static constexpr auto bits_used = 4UL;

    constexpr auto set_value_from_packets(const vector<store_t> &packets) -> void {
        assert(std::size(packets) * bits_used <= bit_sizeof<size_t>);
        value = std::accumulate(begin(packets), end(packets), size_t{}, [&](size_t init, store_t stored) {
            return (init << bits_used) + stored;
        });
    }

    [[nodiscard]] constexpr auto type() const -> TypeId {
        return TypeId{h.type};
    }

    [[nodiscard]] constexpr auto is_value() const -> bool {
        return type() == TypeId::Value;
    }

    [[nodiscard]] constexpr auto eval() const -> size_t {
        if (is_value()) {
            return value;
        }
        return ::eval(type(), *this);
    }

    Header h{};
    size_t value{};
    vector<Packet> subpackets;
};

template<class Func>
constexpr auto visit(Packet &p, Func &&f) -> void {
    std::invoke(f, p);
    if (p.is_value()) {
        return;
    }
    rng::for_each(p.subpackets, [&](auto &sub) { visit(sub, f); });
}

constexpr auto eval(TypeId type_id, const Packet &val) -> size_t {
    const auto &sub = val.subpackets;
    auto for_two = [&](const auto &cmp) -> size_t {
        assert(sub.size() == 2);
        return cmp(sub.front().eval(), sub.back().eval());
    };
    assert(type_id != TypeId::Value);
    switch (type_id) {
        using enum TypeId;
        case Sum: return rng::accumulate(sub, &Packet::eval);
        case Product: return rng::accumulate(sub, &Packet::eval, std::multiplies<>{}, 1UL);
        case Minimum: return rng::min_element(sub, {}, &Packet::eval)->eval();
        case Maximum: return rng::max_element(sub, {}, &Packet::eval)->eval();
        case Greater: return for_two(std::greater<>{});
        case Less: return for_two(std::less<>{});
        case Equal: return for_two(std::equal_to<>{});
        case Value:
        default: assert(false);  // We should land somewhere else
    }
}
constexpr auto parse_packet(size_t index, Packet & /*p*/) -> size_t;

constexpr auto parse_header(size_t first, Header &h) -> size_t {
    h.version = read_n<VTSize>(first);
    h.type = read_n<VTSize>(first);
    return first;
}

constexpr auto parse_value(size_t first, Packet &value) -> size_t {
    vector<store_t> packets;

    auto parse_one = [&]() {
        const auto go_on = read_n<1>(first);
        packets.push_back(read_n<ValueSubPacketSize>(first));
        return go_on != 0;
    };

    while (parse_one()) {}

    value.set_value_from_packets(packets);

    return first;
}

constexpr auto parse_op(size_t first, Packet &op) -> size_t {
    if (op.h.lid == 0) {
        const auto size_info = read_n<BitLengthSize>(first);
        const auto last = first + size_info;
        while (first != last) {
            first = parse_packet(first, op.subpackets.emplace_back());
        }
    } else {
        const auto size_info = read_n<CountLengthSize>(first);
        op.subpackets.reserve(size_info);
        rng::repeat_n(size_info, [&]() {
            first = parse_packet(first, op.subpackets.emplace_back());
        });
    }

    return first;
}

constexpr auto parse_packet(size_t first, Packet &p) -> size_t {
    first = parse_header(first, p.h);
    if (p.is_value()) {
        first = parse_value(first, p);
        return first;
    }

    p.h.lid = read_n<1>(first);
    first = parse_op(first, p);
    return first;
}

constexpr auto part1(std::string_view /*unused*/) {
    Packet root;
    parse_packet(0UL, root);

    auto acc = size_t{};
    visit(root, [&](Packet &p) { acc += p.h.version; });
    return acc;
}

constexpr auto part2(std::string_view /*unused*/) {
    Packet root;
    parse_packet(0UL, root);
    return root.eval();
}

#include "include/main.hpp"
