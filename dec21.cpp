#include "include/advent.hpp"
#include "include/constexpr_maths.hpp"
#include "inputs/dec21.hpp"

constexpr auto num_rolls = 3UL;
constexpr auto length_field = 10UL;
constexpr auto training_score_limit = 1'000UL;

constexpr auto all_pos() {
    return std::views::iota(1UL, length_field + 1);
}

struct TrainingDie {
    static constexpr auto sides = 100;

    [[nodiscard]] constexpr auto roll() -> size_t {
        if (last_roll == sides) {
            last_roll = 0;
        }
        ++num_rolls;
        return ++last_roll;
    }

    [[nodiscard]] constexpr auto roll_n(size_t n) -> size_t {
        size_t result{};
        rng::repeat_n(n, [&]() { result += roll(); });
        return result;
    }

    size_t last_roll{};
    size_t num_rolls{};
};

constexpr auto normalize_pos(size_t &pos) -> size_t {
    pos = pos % length_field;

    if (pos == 0) {
        pos = length_field;
    }

    return pos;
}

struct Player {
    constexpr auto do_turn(TrainingDie &die) -> size_t {
        normalize_pos(position += die.roll_n(num_rolls));

        score += position;
        return position;
    }
    size_t position{};
    size_t score{};
};

constexpr auto read_positions(std::string_view input) -> std::pair<size_t, size_t> {
    getline(input, ':');
    const auto p1 = read_one<size_t>(input, base10).value();
    getline(input, ':');
    const auto p2 = read_one<size_t>(input, base10).value();

    return {p1, p2};
}

template<class Die>
constexpr auto game(std::pair<size_t, size_t> start_pos, size_t score_limit, Die die) {
    Player current{.position = start_pos.first};
    Player other{.position = start_pos.second};

    for (; other.score < score_limit; std::swap(current, other)) {
        current.do_turn(die);
    }

    return std::tuple{current, other, die};
}

constexpr auto part1(std::string_view input) {
    const auto [loosing, winning, die] = game(read_positions(input), training_score_limit, TrainingDie{});
    return loosing.score * die.num_rolls;
}

constexpr auto dirac_score_limit = 21UL;
constexpr auto dirac_die_sides = 3UL;
constexpr auto dirac_score_upper_bound = dirac_score_limit + length_field;

struct CacheKey {
    [[nodiscard]] constexpr auto index() const -> size_t {
        return (((static_cast<unsigned long>(turn_p1) * length_field + p1_pos - 1) * length_field + p2_pos - 1) *
                    dirac_score_upper_bound +
                p1_score) *
                   dirac_score_upper_bound +
               p2_score;
    }

    constexpr static auto max_index() -> size_t {
        return CacheKey{true, length_field, length_field, dirac_score_upper_bound - 1, dirac_score_upper_bound - 1}
            .index();
    }

    constexpr static auto from_index(size_t index) -> CacheKey {
        assert(max_index() >= index);
        CacheKey ret{};
        std::tie(index, ret.p2_score) = div_rest(index, dirac_score_upper_bound);
        std::tie(index, ret.p1_score) = div_rest(index, dirac_score_upper_bound);
        std::tie(index, ret.p2_pos) = div_rest(index, length_field);
        std::tie(index, ret.p1_pos) = div_rest(index, length_field);
        ret.turn_p1 = (index != 0U);
        ++ret.p2_pos;
        ++ret.p1_pos;

        assert(index / 2 == 0);
        return ret;
    }

    [[nodiscard]] constexpr auto next_key(size_t inc) const -> CacheKey {
        CacheKey ret = *this;
        ret.turn_p1 = !turn_p1;
        if (turn_p1) {
            normalize_pos(ret.p1_pos += inc);
            ret.p1_score += ret.p1_pos;
        } else {
            normalize_pos(ret.p2_pos += inc);
            ret.p2_score += ret.p2_pos;
        }
        return ret;
    }

    enum class Standing {
        Open,
        P1Win,
        P2Win,
        Impossible,

    };

    [[nodiscard]] constexpr auto get_standing() const -> Standing {
        using enum Standing;
        const auto p1_won = p1_score >= dirac_score_limit;
        const auto p2_won = p2_score >= dirac_score_limit;

        if (p1_won && p2_won) {
            return Impossible;
        }

        if (p1_won) {
            if (turn_p1) {
                return Impossible;
            }
            return P1Win;
        }

        if (p2_won) {
            if (!turn_p1) {
                return Impossible;
            }
            return P2Win;
        }

        return Open;
    }

    bool turn_p1{};
    size_t p1_pos{};
    size_t p2_pos{};
    size_t p1_score{};
    size_t p2_score{};
};

constexpr auto arbitrary_number = 19743;
static_assert(CacheKey::from_index(arbitrary_number).index() == arbitrary_number);

struct Cache {
    constexpr auto operator[](const CacheKey &key) const -> const std::pair<size_t, size_t> & {
        return wins.at(key.index());
    }

    constexpr auto operator[](const CacheKey &key) -> std::pair<size_t, size_t> & {
        return wins.at(key.index());
    }

    std::array<std::pair<size_t, size_t>, CacheKey::max_index() + 1> wins{};
};

constexpr auto compute_inc_possibilites() {
    std::array<size_t, dirac_die_sides * num_rolls + 1> ret{};
    for (auto throw1: std::views::iota(1UL, dirac_die_sides + 1)) {
        for (auto throw2: std::views::iota(1UL, dirac_die_sides + 1)) {
            for (auto throw3: std::views::iota(1UL, dirac_die_sides + 1)) {
                ++ret.at(throw1 + throw2 + throw3);
            }
        }
    }
    return ret;
}

constexpr auto inc_possibilities = compute_inc_possibilites();

constexpr auto compute_cache(Cache &cache, const CacheKey &key) -> void {
    auto &[this_wins_p1, this_wins_p2] = cache[key];
    switch (key.get_standing()) {
        using enum CacheKey::Standing;
        case P1Win: ++this_wins_p1; return;
        case P2Win: ++this_wins_p2; return;
        case Impossible: return;
        case Open: break;
    }

    for (auto inc: std::views::iota(num_rolls, inc_possibilities.size())) {
        const auto next_key = key.next_key(inc);
        auto [wins_p1, wins_p2] = cache[next_key];
        this_wins_p1 += inc_possibilities[inc] * wins_p1;
        this_wins_p2 += inc_possibilities[inc] * wins_p2;
    }
}

constexpr auto for_max_score(Cache &cache, size_t current_max_score) -> void {
    for (auto lower_score = current_max_score + 1; lower_score-- > 0;) {
        for (auto p1_pos: all_pos()) {
            for (auto p2_pos: all_pos()) {
                for (auto turn_p1: {false, true}) {
                    auto key = CacheKey{turn_p1, p1_pos, p2_pos, lower_score, current_max_score};
                    compute_cache(cache, key);
                    if (lower_score != current_max_score) {
                        std::swap(key.p1_score, key.p2_score);
                        compute_cache(cache, key);
                    }
                }
            }
        }
    }
}

constexpr auto test_all_scores() -> Cache {
    Cache cache;
    for (auto max_score = dirac_score_upper_bound; max_score-- > 0;) {
        for_max_score(cache, max_score);
    }

    return cache;
}

constexpr auto part2(std::string_view input) -> size_t {
    const auto [p1_pos, p2_pos] = read_positions(input);
    const CacheKey start_key{.turn_p1 = true, .p1_pos = p1_pos, .p2_pos = p2_pos};

    const auto cache = test_all_scores();
    const auto [wins_p1, wins_p2] = cache[start_key];
    return rng::max(wins_p1, wins_p2);
}

#include "include/main.hpp"
