#include "include/advent.hpp"
#include "inputs/dec2.hpp"
#include <optional>

using namespace std::string_view_literals;

enum Direction : long {
    forward,
    down,
    up,
};

static constexpr auto dir_string = {"forward"sv, "down"sv, "up"sv};

constexpr auto read_direction(std::string_view &inp) -> std::optional<std::pair<Direction, int>> {
    auto buffer = read_one(inp);
    auto amount = read_one<int>(inp, base10);

    if (buffer.empty() || !amount) {
        return {};
    }

    const auto *it = rng::find(dir_string, buffer);
    auto dir = Direction{std::distance(begin(dir_string), it)};

    return {{dir, *amount}};
}

template<class Func>
constexpr auto for_each_direction(Func &&f, std::string_view inp) {
    while (true) {
        auto temp = read_direction(inp);
        if (!temp) {
            break;
        }
        std::apply(f, *temp);
    }
}

constexpr auto part1(std::string_view inp) -> int {
    int x{};
    int y{};

    for_each_direction(
        [&](auto dir, auto amount) {
            switch (dir) {
                case forward: x += amount; break;
                case down: y += amount; break;
                case up: y -= amount; break;
            }
        },
        inp);
    return x * y;
}

constexpr auto part2(std::string_view inp) -> int {
    int x{};
    int y{};
    int aim{};
    for_each_direction(
        [&](auto dir, auto amount) {
            switch (dir) {
                case forward:
                    x += amount;
                    y += aim * amount;
                    break;
                case down: aim += amount; break;
                case up: aim -= amount; break;
            }
        },
        inp);

    return x * y;
}

#include "include/main.hpp"
