#include "include/advent.hpp"
#include "inputs/dec10.hpp"
#include <variant>

enum Direction { open, close };
enum CheckResult { valid, incomplete, corrupted };
constexpr auto opening = "([{<"sv;
constexpr auto closing = ")]}>"sv;

constexpr auto Points = std::array{3, 57, 1197, 25137};

using stack_t = vector<size_t>;

constexpr auto get_direction(char c) -> Direction {
    if (opening.find(c) != std::string_view::npos) {
        return open;
    }
    return close;
}

constexpr auto consume(char c, stack_t &stack) -> bool {
    const auto dir = get_direction(c);
    if (dir == open) {
        stack.push_back(opening.find(c));
        return true;
    }
    if (stack.empty()) {
        return false;
    }
    const auto counterpart = stack.back();
    stack.pop_back();
    return counterpart == closing.find(c);
}

constexpr auto inc_completion_score(size_t sofar, size_t index) {
    return sofar * (closing.size() + 1) + index + 1;
}

constexpr auto check(std::string_view s) -> std::variant<std::monostate, size_t, char> {
    stack_t stack;
    const auto *const unconsumed = rng::find_if(s, [&](char c) { return !consume(c, stack); });
    if (unconsumed != end(s)) {
        return *unconsumed;
    }
    if (stack.empty()) {
        return {};
    }
    return std::accumulate(rbegin(stack), rend(stack), 0UL, inc_completion_score);
}

constexpr auto median(stack_t &&s) {
    auto middle_it = std::next(begin(s), ssize(s) / 2);
    std::nth_element(begin(s), middle_it, end(s));
    return *middle_it;
}

template<class Func>
constexpr auto for_each_line(std::string_view &inp, Func &&func) {
    std::string_view buffer;
    while (!(buffer = read_one(inp)).empty()) {
        const auto result = check(buffer);
        func(result);
    }
}

constexpr auto part1(std::string_view inp) {
    auto point_sum = 0;
    for_each_line(inp, [&](const auto &result) {
        if (result.index() == corrupted) {
            const auto c = std::get<corrupted>(result);
            assert(get_direction(c) == close);
            point_sum += Points.at(closing.find(c));
        }
    });
    return point_sum;
}

constexpr auto part2(std::string_view inp) {
    stack_t points;
    for_each_line(inp, [&](const auto result) {
        if (result.index() == incomplete) {
            points.push_back(std::get<incomplete>(result));
        }
    });
    return median(std::move(points));
}

#include "include/main.hpp"
