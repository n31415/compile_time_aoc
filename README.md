I want to try to do as much as possible of advent of code 2021 at compile time with C++.


## Compiling

The compiler support for C++20 is still a bit patchy, so if this will work for you depends on which compiler you use. I tested it with GCC version 11.1.0 (of the Arch Linux package gcc 11.1.0-3) and the included libstdc++. 

It nearly works for clang 13.0.0, but there in my version of libstdc++ there is one `typename` missing that clang cannot cope with. This should actually be fixed in GCC 11.1.0 (https://gcc.gnu.org/bugzilla/show_bug.cgi?id=100900) but it is not for me.

If your compiler works, you can just copy your session cookie from adventofcode.com into the appropriate field in `aoc_cookie.txt.template` and remove the `.template` suffix. Then `make dec1` will download the input and build the program for the 1st of December. 

Be advised that I set -fconstexpr-ops-limit absurdly high for the more challenging problems. Your computer might take more on than it can chew! The standard -fconstexpr-ops-limit does work for more than half of the problems, though.
