#include "include/advent.hpp"
#include "include/field.hpp"
#include "inputs/dec11.hpp"

constexpr char flash_level = 10;
constexpr auto flash(Field<> &f, const Point2D &p, vector<char> &visited, vector<Point2D> &to_visit) -> bool {
    if (f.twod_access(p, visited) != 0) {
        return false;
    }
    f.twod_access(p, visited) = 1;
    auto flash_neighbor = [&](const Point2D n) {
        const auto new_level = ++f.get_content(n);
        if (new_level >= flash_level && f.twod_access(n, visited) == 0) {
            to_visit.push_back(n);
        }
    };
    f.for_each_neighbor(p, flash_neighbor);
    f.for_each_diagonal(p, flash_neighbor);
    return true;
}

constexpr auto inc_field(Field<> &f, vector<Point2D> &to_visit) {
    f.for_all_points([&](const Point2D p) {
        const auto new_level = ++f.get_content(p);
        if (new_level >= flash_level) {
            to_visit.push_back(p);
        }
    });
}

constexpr auto flash_round(Field<> &f, vector<char> &visited) -> size_t {
    auto num_flashes = 0UL;
    vector<Point2D> to_visit;
    rng::for_each(visited, [](auto &c) { c = 0; });
    inc_field(f, to_visit);
    while (!to_visit.empty()) {
        const auto next_p = to_visit.back();
        to_visit.pop_back();
        num_flashes += static_cast<unsigned long>(flash(f, next_p, visited, to_visit));
    }
    rng::for_each(f.content, [&](auto &con) {
        if (con >= flash_level) {
            con = 0;
        }
    });
    return num_flashes;
}

constexpr auto part1(std::string_view input) {
    constexpr auto num_turns = 100UL;
    auto field = read2d<size_t>(input);
    auto visited = field.create_2d<char>(0);
    auto num_flashes = 0UL;
    rng::repeat_n(num_turns, [&]() { num_flashes += flash_round(field, visited); });

    return num_flashes;
}

constexpr auto part2(std::string_view input) {
    auto field = read2d<size_t>(input);
    auto visited = field.create_2d<char>(0);
    for (auto i = 0UL;; ++i) {
        if (flash_round(field, visited) == field.content.size()) {
            return i + 1;
        }
    }
}

#include "include/main.hpp"
