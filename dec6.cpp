#include "include/advent.hpp"
#include "inputs/dec6.hpp"

constexpr auto rep_cycle = 7UL;
constexpr auto initial_rep_cycle = 9UL;
constexpr auto num_ticks1 = 80UL;
constexpr auto num_ticks2 = 256UL;

constexpr auto count_fishes(std::string_view inp, size_t num_ticks) {
    auto initial_nums = read_vector_comma_sep<size_t>(inp, base10);
    vector<size_t> birthing_fishes(num_ticks + initial_rep_cycle);
    size_t num_fishes = initial_nums.size();

    for (auto num: initial_nums) {
        ++birthing_fishes[num];
    }

    for (auto day = 0UL; day < num_ticks; ++day) {
        const auto births = birthing_fishes[day];
        num_fishes += births;
        birthing_fishes[day + initial_rep_cycle] += births;
        birthing_fishes[day + rep_cycle] += births;
    }

    return num_fishes;
}

constexpr auto part1(std::string_view inp) -> size_t {
    return count_fishes(inp, num_ticks1);
}

constexpr auto part2(std::string_view inp) -> size_t {
    return count_fishes(inp, num_ticks2);
}

#include "include/main.hpp"
