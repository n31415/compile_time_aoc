#include "include/advent.hpp"
#include "include/utility.hpp"
#include "inputs/dec13.hpp"
#include <bits/ranges_algo.h>

struct Fold {
    long value;
    bool vertical;
};

constexpr auto empty = '.';
constexpr auto dot = '#';
constexpr auto num_lines = 6;
constexpr auto dots_per_letter = 5;
constexpr auto letters_per_line = 8;
constexpr auto line_length = dots_per_letter * letters_per_line + 1;
constexpr auto num_output_chars = num_lines * line_length;

using input_t = std::pair<vector<Point2D>, vector<Fold>>;

constexpr auto read_instructions(std::string_view &sv) -> input_t {
    input_t ret;
    remove_ws(sv);
    {
        const auto num_lines = rng::count(sv, '\n');
        auto &points = ret.first;
        points.reserve(num_lines);

        while (true) {
            std::string_view line = getline(sv);
            auto x = read_one<long>(line, base10);
            if (!line.empty()) {
                line.remove_prefix(1);
            }
            auto y = read_one<long>(line, base10);

            if (!x || !y) {
                break;
            }

            points.push_back({*x, *y});
        }
    }

    {
        while (true) {
            std::string_view line = getline(sv);
            const auto pos = line.find_first_of("xy");
            if (pos == std::string_view::npos) {
                break;
            }
            Fold f{};
            f.vertical = line[pos] == 'x';
            line.remove_prefix(pos + 2);
            f.value = *read_one<long>(line, base10);
            ret.second.push_back(f);
        }
    }
    return ret;
}

constexpr auto move_dot(char &dest, char &source) {
    if (source == dot) {
        dest = std::exchange(source, empty);
    }
}

constexpr auto do_fold(vector<Point2D> &points, Fold fold) {
    auto get_mem = [vert = fold.vertical](auto &&p) -> auto && {
        return vert ? p.x() : p.y();
    };
    for (auto &p: points) {
        if (get_mem(p) > fold.value) {
            get_mem(p) = 2 * fold.value - get_mem(p);
        }
    }
}

constexpr auto part1(std::string_view sv) -> size_t {
    auto inp = read_instructions(sv);
    do_fold(inp.first, inp.second.front());

    rng::sort(inp.first);
    auto last = std::begin(rng::unique(inp.first));
    return rng::distance(inp.first.begin(), last);
}

constexpr auto point_to_output(const Point2D &p) -> size_t {
    return p.x() + p.y() * line_length;
}

constexpr auto part2(std::string_view sv) -> std::array<char, num_output_chars> {
    auto inp = read_instructions(sv);
    for (const auto &fold: inp.second) {
        do_fold(inp.first, fold);
    }

    std::array<char, num_output_chars> ret{};
    ret.fill(empty);
    for (const auto &p: inp.first) {
        ret.at(point_to_output(p)) = dot;
    }

    for (const auto i: std::views::iota(1UL, num_lines + 1UL)) {
        ret.at(i * line_length - 1) = '\n';
    }

    return ret;
}

auto main() -> int {
    MAYBE_CONSTEXPR auto p1 = part1(input);
    MAYBE_CONSTEXPR auto p2 = part2(input);

    println("Part 1:", p1);
    println("Part 2:\n");
    println(std::string_view(begin(p2), end(p2)));
}
