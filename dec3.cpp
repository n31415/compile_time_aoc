#include "include/advent.hpp"
#include "include/io.hpp"
#include "inputs/dec3.hpp"

constexpr auto count_ones(std::string_view input, vector<int> &output) {
    rng::transform(input, output, begin(output), [](auto bit, auto count) { return count + (bit == '1'); });
}

constexpr auto compute_gamma_epsilon(const vector<int> &num_ones, const int total_lines) -> std::pair<size_t, size_t> {
    return rng::accumulate(
        num_ones,
        [&](const auto elem) { return 2 * elem > total_lines; },
        [&](const auto init, const bool common) {
            return std::pair{init.first * 2 + common, init.second * 2 + !common};
        },
        std::pair<size_t, size_t>{});
}

constexpr auto most_common(const vector<std::string_view> &nums, size_t position, bool invert) {
    const size_t num_ones = rng::count_if(nums, [&](const auto &num) { return num[position] == '1'; });
    return (2 * num_ones < nums.size()) ^ invert ? '0' : '1';
}

constexpr auto find_number(vector<std::string_view> nums, bool invert) -> std::string_view {
    auto pos = 0UL;
    while (nums.size() > 1) {
        const auto common_char = most_common(nums, pos, invert);
        erase_if(nums, [&](const auto &num) { return num[pos] != common_char; });
        ++pos;
    }

    return nums.front();
}

constexpr auto part1(std::string_view inp) -> size_t {
    auto no_lines{1};
    std::string_view buff = read_one(inp);
    vector<int> num_ones(buff.length());
    while (!(buff = read_one(inp)).empty()) {
        count_ones(buff, num_ones);
        ++no_lines;
    }

    auto [gamma, epsilon] = compute_gamma_epsilon(num_ones, no_lines);
    return gamma * epsilon;
}

constexpr auto part2(std::string_view inp) -> size_t {
    auto binary_nums = read_vector(inp);
    auto oxy = find_number(binary_nums, false);
    auto co2 = find_number(binary_nums, true);

    return *read_one<size_t>(oxy, 2) * *read_one<size_t>(co2, 2);
}

#include "include/main.hpp"
