#include "include/advent.hpp"
#include "include/flatmap.hpp"
#include "inputs/dec12.hpp"
#include <iterator>

using pre_edge_t = std::pair<std::string_view, std::string_view>;
constexpr auto start_node = "start"sv;
constexpr auto end_node = "end"sv;

struct Graph {
    using edge_id_t = size_t;
    using node_id_t = size_t;
    struct Node {
        std::string_view name;
        vector<edge_id_t> incident_edges;

        [[nodiscard]] constexpr auto is_small() const -> bool {
            const auto c = name.front();
            return 'a' <= c && c <= 'z';
        }

        [[nodiscard]] constexpr auto is_end() const -> bool {
            return name == end_node;
        }
    };

    struct Edge {
        [[nodiscard]] constexpr auto other(node_id_t id) const -> node_id_t {
            if (id == nodes.front()) {
                return nodes.back();
            }
            return nodes.front();
        }
        std::array<node_id_t, 2> nodes;
    };

    vector<Node> nodes;
    vector<Edge> edges;
};

using visited_t = vector<int>;

constexpr auto read_edge(std::string_view &in) -> pre_edge_t {
    pre_edge_t ret;
    remove_ws(in);
    ret.first = getline(in, '-');
    ret.second = getline(in);

    return ret;
}

template<size_t index>
constexpr auto pair_to_map(const pre_edge_t &pair) {
    return std::pair{std::get<index>(pair), 0UL};
};

constexpr auto create_ids(const vector<pre_edge_t> &vec) -> dynamic_flatmap_t<std::string_view, size_t> {
    dynamic_flatmap_t<std::string_view, size_t> to_id;
    rng::transform(vec, std::back_inserter(to_id), &pair_to_map<0>);
    rng::transform(vec, std::back_inserter(to_id), &pair_to_map<1>);

    auto id_count = 2UL;
    for (auto &[key, value]: to_id) {
        if (key == start_node) {
            value = 0UL;
        } else if (key == end_node) {
            value = 1UL;
        } else {
            value = id_count++;
        }
    }
    return to_id;
}

constexpr auto read_graph(std::string_view &in) {
    vector<pre_edge_t> vec;
    pre_edge_t buff;
    while (!(buff = read_edge(in)).second.empty()) {
        vec.push_back(std::move(buff));
    }

    const auto to_id = create_ids(vec);
    Graph g;
    g.nodes.resize(to_id.size());

    for (const auto &[key, value]: to_id) {
        g.nodes[value].name = key;
    }

    for (const auto &[first, second]: vec) {
        const auto id0 = flat_at(to_id, first);
        const auto id1 = flat_at(to_id, second);
        const auto edge_id = g.edges.size();
        g.edges.push_back(Graph::Edge{{id0, id1}});
        g.nodes[id0].incident_edges.push_back(edge_id);
        g.nodes[id1].incident_edges.push_back(edge_id);
    }

    return g;
}

constexpr auto recursive(const Graph &g, visited_t &current_path, size_t cur_node_id, const bool allow_twice)
    -> size_t {
    if (cur_node_id == 1UL) {
        return 1UL;
    }
    auto paths_from_here = 0UL;
    for (const auto edge_id: g.nodes[cur_node_id].incident_edges) {
        const auto other_node_id = g.edges[edge_id].other(cur_node_id);
        const auto is_small = g.nodes[other_node_id].is_small();
        auto is_visited = current_path[other_node_id] != 0;
        if (is_visited && other_node_id == 0UL) {
            // Do not visit start node twice
            continue;
        }
        if (!is_small || !is_visited || allow_twice) {
            ++current_path[other_node_id];
            paths_from_here += recursive(g, current_path, other_node_id, (!is_small || !is_visited) && allow_twice);
            --current_path[other_node_id];
        }
    }

    return paths_from_here;
}

constexpr auto part1(std::string_view in) {
    const auto graph = read_graph(in);
    visited_t p(graph.nodes.size(), 0);
    p.front() = 1;

    return recursive(graph, p, 0UL, false);
}
constexpr auto part2(std::string_view in) {
    const auto graph = read_graph(in);
    visited_t p(graph.nodes.size(), 0);
    p.front() = 1;

    return recursive(graph, p, 0UL, true);
}

#include "include/main.hpp"
