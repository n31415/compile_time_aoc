#include "include/advent.hpp"
#include "include/field.hpp"
#include "inputs/dec9.hpp"
#include <string_view>

constexpr auto hight_point = 9UL;

[[nodiscard]] constexpr auto is_local_low(const Field<> &f, const Point2D &p) -> bool {
    bool is_low = true;
    const auto here = f.get_content(p);
    f.for_each_neighbor(p, [&](const auto &np) { is_low = is_low && f.get_content(np) > here; });
    return is_low;
}

constexpr auto part1(std::string_view sv) {
    const auto floor = read2d<size_t>(sv);
    auto risk = 0UL;
    floor.for_all_points([&](const auto p) {
        if (is_local_low(floor, p)) {
            risk += floor.get_content(p) + 1;
        }
    });
    return risk;
}

constexpr auto basin_size(const Point2D &p, vector<char> &visited, const Field<> &floor) {
    auto size = 0UL;
    vector<Point2D> stack{p};
    while (!stack.empty()) {
        const auto cur_p = stack.back();
        stack.pop_back();
        if (floor.twod_access(cur_p, visited) != 0) {
            continue;
        }

        floor.twod_access(cur_p, visited) = 1;
        ++size;
        floor.for_each_neighbor(cur_p, [&](const Point2D p) {
            if (floor.get_content(p) < hight_point) {
                stack.push_back(p);
            }
        });
    }
    return size;
}

constexpr auto part2(std::string_view sv) {
    const auto floor = read2d<size_t>(sv);
    vector<Point2D> low_points;
    floor.for_all_points([&](const auto p) {
        if (is_local_low(floor, p)) {
            low_points.push_back(p);
        }
    });

    vector<size_t> basin_sizes;
    vector<char> visited(floor.content.size(), 0);
    rng::transform(
        low_points, std::back_inserter(basin_sizes), [&](const Point2D p) { return basin_size(p, visited, floor); });

    rng::sort(basin_sizes, std::greater<>{});
    return std::accumulate(begin(basin_sizes), begin(basin_sizes) + 3, 1, std::multiplies<>{});
}

#include "include/main.hpp"
