CXX := g++
CXXFLAGS = -Wall -Wextra -Werror -pedantic -g -std=c++20 -fconstexpr-ops-limit=1099511627776
#																				2^40

INPUT_HEADERS := $(wildcard inputs/*hpp)
INCLUDE_HEADERS := $(wildcard include/*hpp)
DEC_CPPS := $(wildcard dec*.cpp)
DEC_EXECUTABLES := $(subst .cpp,,$(DEC_CPPS))
TODAY := $(shell date +%-d)

inputs/dec%.hpp:
	./get_input.sh $*

dec%_runtime: dec%.cpp inputs/dec%.hpp $(INCLUDE_HEADERS)
	$(CXX) $(CXXFLAGS) $< -o $@
	./$@

dec%: dec%.cpp dec%_runtime
	$(CXX) $(CXXFLAGS) -DALL_THE_THINGS $< -o $@
	./$@

.PHONY=all
all: $(DEC_EXECUTABLES)

clean:
	rm $(DEC_EXECUTABLES)
	rm dec*_runtime

.PHONY=today
today: dec$(TODAY)

.PRECIOUS: inputs/dec%.hpp dec%_runtime
