#include "include/advent.hpp"
#include "inputs/dec7.hpp"
#include <cstddef>
#include <limits>

constexpr auto gauss(size_t n) -> size_t {
    return n * (n + 1) / 2;
}

template<class DistProj>
constexpr auto calc_dist(size_t pos, const vector<size_t> &positions, DistProj &&proj, size_t alpha) -> size_t {
    auto acc = size_t{};
    for (const auto next: positions) {
        acc += proj(distance(next, pos));
        if (acc >= alpha) {
            break;
        }
    }

    return acc;
}

template<class DistProj>
constexpr auto get_min_dist(const vector<size_t> &positions, DistProj &&proj) -> size_t {
    const auto max_position = *rng::max_element(positions);
    auto min_val = std::numeric_limits<size_t>::max();
    for (auto pos = size_t{}; pos <= max_position; ++pos) {
        update_min(min_val, calc_dist(pos, positions, proj, min_val));
    }
    return min_val;
}

constexpr auto part1(std::string_view input) -> size_t {
    const auto positions = read_vector_comma_sep<size_t>(input, base10);
    return get_min_dist(positions, std::identity{});
}

constexpr auto part2(std::string_view input) -> size_t {
    const auto positions = read_vector_comma_sep<size_t>(input, base10);
    return get_min_dist(positions, &gauss);
}

#include "include/main.hpp"
