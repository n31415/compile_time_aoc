#!/bin/sh

DAY=${1:-$(date +%-d)}
FNAME="inputs/dec$DAY.hpp"

echo '#include <string_view>
constexpr auto input = std::string_view{ R"DELIM( 
' > $FNAME

wget -o /dev/null --load-cookies aoc_cookie.txt https://adventofcode.com/2021/day/$DAY/input -O - >> $FNAME

echo ')DELIM"};' >> $FNAME


