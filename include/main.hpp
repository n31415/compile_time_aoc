#pragma once
#include "advent.hpp"

// This has to be included at the bottom of the cpp file, since we cannot predeclare constexpr functions.

auto main() -> int {
    MAYBE_CONSTEXPR auto p1 = part1(input);
    MAYBE_CONSTEXPR auto p2 = part2(input);

    println("Part 1:", p1);
    println("Part 2:", p2);
}
