#pragma once

#include <iostream>
#include "defs.hpp"
#include <type_traits>

template<class... Args>
constexpr auto println([[maybe_unused]] Args &&...args) -> void {
    if (!std::is_constant_evaluated()) {
        ((std::cout << args << ' '), ...);
        std::cout << '\n';
    }
}
