#pragma once
#include <optional>
#include <string_view>
#include <type_traits>

#include "algorithms.hpp"
#include "constexpr_vector.hpp"
#include "constexpr_char_conv.hpp"
#include "constexpr_maths.hpp"
#include "utility.hpp"

template<class Func>
constexpr auto discard_while(std::string_view &sv, Func &&f) -> void requires std::is_invocable_r_v<bool, Func, char> {
    while (!sv.empty() && f(sv.front())) {
        sv.remove_prefix(1);
    }
}

// Expect and extract a character but fail silently if the view is empty.
constexpr auto expect(std::string_view &sv, char c) -> void {
    if (!sv.empty()) {
        assert(sv.front() == c);
        sv.remove_prefix(1);
    }
}

constexpr auto skip_to_integral(std::string_view &sv) -> void {
    discard_while(sv, [](const char c) { return digit_to_number(c, base10) == -1 && c != '-'; });
};

inline constexpr auto Whitespaces = std::string_view(" \n\r\t\v\f");

constexpr auto is_ws(char c) -> bool {
    return Whitespaces.find(c) != std::string_view::npos;
}

constexpr auto remove_ws(std::string_view &sv) -> void {
    auto pos = sv.find_first_not_of(Whitespaces);
    sv.remove_prefix(std::min(pos, sv.size()));
}

constexpr auto getline(std::string_view &sv, char delim = '\n') -> std::string_view {
    const auto endline_pos = sv.find(delim);
    const auto ret = sv.substr(0, endline_pos);
    sv.remove_prefix(std::min(sv.size(), endline_pos + 1));
    return ret;
}

constexpr auto get_char(std::string_view &sv) -> char {
    if (sv.empty()) {
        return {};
    }
    auto ret = sv.front();
    sv.remove_prefix(1);
    return ret;
}

template<arithmetic T>
constexpr auto read_one(std::string_view &sv, int base = base10) -> std::optional<T> {
    T t{};
    remove_ws(sv);
    auto [new_view, ec] = from_view(sv, t, base);
    if (ec != std::errc{}) {
        return {};
    }
    sv = new_view;
    return t;
}

constexpr auto read_one(std::string_view &sv) -> std::string_view {
    const auto start_pos = std::min(sv.size(), sv.find_first_not_of(Whitespaces));
    const auto end_pos = std::min(sv.size(), sv.find_first_of(Whitespaces, start_pos));

    const auto ret = sv.substr(start_pos, end_pos - start_pos);
    sv.remove_prefix(end_pos);
    return ret;
}

template<class T, class Reader>
constexpr auto read_one(std::string_view &sv, Reader &&reader) -> std::optional<T> {
    remove_ws(sv);
    auto ret = reader(sv);
    return ret;
}

template<class T, class Reader>
constexpr auto read_vector(std::string_view &sv, Reader &&reader) -> vector<T> {
    vector<T> ret;

    for (std::optional<T> buf; (buf = read_one<T>(sv, reader));) {
        ret.emplace_back(*buf);
    }
    return ret;
}

constexpr auto read_vector(std::string_view &sv) -> vector<std::string_view> {
    vector<std::string_view> ret;

    for (std::string_view buf; !(buf = read_one(sv)).empty();) {
        ret.emplace_back(buf);
    }
    return ret;
}

template<class T, class Reader>
constexpr auto read_vector_comma_sep(std::string_view &sv, Reader &&reader) -> vector<T> {
    vector<T> ret;

    for (std::optional<T> buf; (buf = read_one<T>(sv, reader));) {
        ret.push_back(*buf);
        sv.remove_prefix(1);
    }
    return ret;
}
