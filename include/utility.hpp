#pragma once
#include <functional>
#include <type_traits>
#include <utility>

#include "algorithms.hpp"
#include "constexpr_maths.hpp"

constexpr auto bits_to_the_byte = size_t{8};
template<class T>
constexpr auto bit_sizeof = sizeof(T) * bits_to_the_byte;

template<class T, class U, class Cmp = std::less<>>
constexpr auto update_min(T &t, U &&u, Cmp &&cmp = {}) -> T &requires std::is_invocable_r_v<bool, Cmp, U, T> {
    if (cmp(u, t)) {
        t = std::forward<U>(u);
    }
    return t;
}

template<class T, class U, class Cmp = std::less<>>
constexpr auto update_max(T &t, U &&u, Cmp &&cmp = {}) -> T &requires std::is_invocable_r_v<bool, Cmp, T, U> {
    if (cmp(t, u)) {
        t = std::forward<U>(u);
    }
    return t;
}

template<class... Ts>
struct overloaded : Ts... {
    using Ts::operator()...;
};

constexpr inline auto exchange_view_begin(std::string_view &sv, const char *const new_begin) -> decltype(begin(sv)) {
    const auto *ret = begin(sv);
    sv.remove_prefix(rng::distance(ret, new_begin));
    return ret;
}

constexpr inline auto exchange_view_end(std::string_view &sv, const char *const new_end) -> decltype(end(sv)) {
    const auto *ret = end(sv);
    sv.remove_prefix(rng::distance(new_end, ret));
    return ret;
}

template<class Source, class Dest>
using copy_const_t = std::conditional_t<std::is_const_v<Source>, std::add_const_t<Dest>, std::remove_const_t<Dest>>;

constexpr auto signum(const std::integral auto &inp) -> int {
    if (inp < 0) {
        return -1;
    }
    if (inp > 0) {
        return 1;
    }
    return 0;
}

template<size_t MaxIndex, class Func>
constexpr auto for_each_index(Func &&func) {
    func.template operator()<MaxIndex>();
    if constexpr (MaxIndex > 0) {
        for_each_index<MaxIndex - 1>(std::forward<Func>(func));
    }
}

template<size_t Dimension>
struct PointNd {
    static constexpr auto dimension = Dimension;

    [[nodiscard]] constexpr auto x() const -> const long &requires(dimension >= 1) {
        return std::get<0>(coords);
    }
    constexpr auto x() -> long &requires(dimension >= 1) {
        return std::get<0>(coords);
    }

    [[nodiscard]] constexpr auto y() const -> const long &requires(dimension >= 2) {
        return std::get<1>(coords);
    }
    constexpr auto y() -> long &requires(dimension >= 2) {
        return std::get<1>(coords);
    }

    [[nodiscard]] constexpr auto z() const -> const long &requires(dimension >= 3) {
        return std::get<2>(coords);
    }
    constexpr auto z() -> long &requires(dimension >= 3) {
        return std::get<2>(coords);
    }

    [[nodiscard]] constexpr auto at(size_t index) const -> long {
        return coords.at(index);
    }
    constexpr auto at(size_t index) -> long & {
        return coords.at(index);
    }

    template<size_t I>
    [[nodiscard]] constexpr auto proj() const -> long requires(I < dimension) {
        return std::get<I>(coords);
    }

    static constexpr auto for_each_coord(auto &&func) {
        for_each_index<dimension - 1>(std::forward<decltype(func)>(func));
    }

    constexpr auto operator<=>(const PointNd<Dimension> &) const = default;

    std::array<long, dimension> coords;
};
using Point2D = PointNd<2>;

template<class T>
struct is_point_struct {
    static constexpr auto value = false;
};
template<size_t D>
struct is_point_struct<PointNd<D>> {
    static constexpr auto value = true;
};
template<class T>
static constexpr auto is_point_v = is_point_struct<T>::value;

template<class T>
concept is_point = is_point_v<std::remove_cvref_t<T>>;

template<size_t Index, class Point>
constexpr auto get(Point &&p) -> auto &requires(is_point<Point>) {
    return std::get<Index>(p.coords);
}

[[nodiscard]] constexpr auto manhattan(const Point2D &p, const Point2D &q) -> long {
    return distance(q.x(), p.x()) + distance(q.y(), p.y());
}

template<class Func, class First, class... Args>
constexpr auto call_longest(Func &&func, First &&first, Args &&...args) -> decltype(auto) {
    if constexpr (std::is_invocable_v<Func, First, Args...>) {
        return std::invoke(std::forward<Func>(func), std::forward<First>(first), std::forward<Args>(args)...);
    } else {
        return call_longest(std::forward<Func>(func), std::forward<Args>(args)...);
    }
}
