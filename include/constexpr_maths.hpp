#pragma once
#include <concepts>
#include <type_traits>

#include "algorithms.hpp"

template<class T>
concept arithmetic = std::is_arithmetic_v<T>;

template<arithmetic base_t>
constexpr auto pow(const base_t base, const std::integral auto exp) -> base_t {
    if (exp < 0) {
        return base_t{1} / pow(base, -exp);
    }

    auto ret = pow(base, exp / 2);
    ret *= ret;

    if (exp % 2 == 1) {
        ret *= base;
    }

    return ret;
}

template<arithmetic arithmetic_t>
constexpr auto abs(const arithmetic_t input) -> arithmetic_t {
    if constexpr (std::is_unsigned_v<arithmetic_t>) {
        return input;
    }

    if (input < arithmetic_t{}) {
        return -input;
    }
    return input;
}

template<arithmetic T>
constexpr auto distance(T t1, T t2) -> T {
    if (t1 < t2) {
        return t2 - t1;
    }
    return t1 - t2;
}

template<std::integral T>
constexpr auto sqrt_ceil(T i) -> T {
    assert(i >= 0);
    if (i <= 2) {
        return i;
    }
    return *rng::lower_bound(std::views::iota(0, i), i, {}, [](const auto j) { return j * j; });
}

template<std::integral T1, std::integral T2>
struct div_rest_result {
    using common_type = std::common_type_t<T1, T2>;

    constexpr operator std::tuple<common_type &, common_type &>() {
        return std::tie(quot, rem);
    }

    common_type quot;
    common_type rem;
};

template<std::integral T1, std::integral T2>
constexpr auto div_rest(T1 dividend, T2 divisor) -> div_rest_result<T1, T2> {
    return {.quot = dividend / divisor, .rem = dividend % divisor};
}
