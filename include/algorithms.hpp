#pragma once

#include <algorithm>
#include <cassert>
#include <functional>
#include <iterator>
#include <numeric>
#include <ranges>
#include <type_traits>

namespace rng {
    using namespace std::ranges;
struct rng_acc_fn {
    template<input_range Rng, class Proj = std::identity, class BinOp = std::plus<>,
             class Val = typename std::projected<iterator_t<Rng>, Proj>::value_type>
    constexpr auto operator()(Rng &&r, Proj proj = {}, BinOp op = {}, Val val = {}) const
        -> std::invoke_result_t<BinOp, Val, std::indirect_result_t<Proj, rng::iterator_t<Rng>>> {
        return std::accumulate(begin(r), end(r), val, [&](Val &&val, auto &&elem) {
            return op(std::move(val), std::invoke(proj, std::forward<decltype(elem)>(elem)));
        });
    }
};

inline constexpr rng_acc_fn accumulate;

// This works slightly different than std::adjacent_difference and it is correct so!
// (std::adjacent_difference has *first as the first output, which is not a difference between adjacent elements)
struct rng_adj_dif_fn {
    template<input_range Rng, std::weakly_incrementable O, class Proj = std::identity, class BinOp = std::minus<>>
    constexpr auto operator()(Rng &&r, O out, BinOp op = {}, Proj proj = {}) const
        -> in_out_result<iterator_t<Rng>, O> requires
        std::indirectly_writable<O, std::indirect_result_t<BinOp &, std::projected<iterator_t<Rng>, Proj>,
                                                           std::projected<iterator_t<Rng>, Proj>>> {
        assert(!empty(r));
        auto first = begin(r);
        auto last = end(r);
        auto last_val = proj(*first);
        while (++first != last) {
            auto val = proj(*first);
            *out++ = std::invoke(op, val, std::move(last_val));
            last_val = std::move(val);
        }

        return in_out_result{last, out};
    }
};

inline constexpr rng_adj_dif_fn adjacent_difference;

template<class Func>
constexpr auto repeat_n(size_t n, Func &&f) -> void {
    for (auto i = 0UL; i < n; ++i) {
        f();
    }
}

struct find_pos_fn {
    template<std::input_iterator I, std::sentinel_for<I> S, class T, class Proj = std::identity>
    requires std::indirect_binary_predicate<equal_to, std::projected<I, Proj>, const T *> constexpr auto
        operator()(I first, S last, const T &value, Proj proj = {}) const -> std::iter_difference_t<I> {
        return rng::distance(first, rng::find(first, last, value, proj));
    }

    template<input_range R, class T, class Proj = std::identity>
    requires std::indirect_binary_predicate<equal_to, std::projected<iterator_t<R>, Proj>, const T *> constexpr auto
        operator()(R &&r, const T &value, Proj proj = {}) const -> range_difference_t<R> {
        return (*this)(begin(r), end(r), value, std::ref(proj));
    }
};

inline constexpr find_pos_fn find_pos;

struct find_if_pos_fn {
    template<std::input_iterator I, std::sentinel_for<I> S, class Proj = std::identity,
             std::indirect_unary_predicate<std::projected<I, Proj>> Pred>
    constexpr auto operator()(I first, S last, Pred &&pred, Proj proj = {}) const -> std::iter_difference_t<I> {
        return std::distance(first, rng::find_if(first, last, std::forward<Pred>(pred), proj));
    }

    template<input_range R, class Proj = std::identity,
             std::indirect_unary_predicate<std::projected<iterator_t<R>, Proj>> Pred>
    constexpr auto operator()(R &&r, Pred &&pred, Proj proj = {}) const -> range_difference_t<R> {
        return (*this)(begin(r), end(r), std::forward<Pred>(pred), std::ref(proj));
    }
};

inline constexpr find_if_pos_fn find_if_pos;

template<class T, class... Args, class Ret = std::common_type_t<Args...>>
constexpr auto nearest(T &&target, Args &&...candidates) -> Ret {
    return rng::min({candidates...}, {}, [&](const auto &arg) { return distance(target, arg); });
}
}  // namespace rng
