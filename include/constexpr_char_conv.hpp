#pragma once
#include <concepts>
#include <string_view>
#include <system_error>
#include <type_traits>

#include "constexpr_maths.hpp"

constexpr auto base10 = int{10};

struct from_chars_result {
    explicit constexpr operator bool() const {
        return ec == std::errc{};
    }
    const char *ptr{nullptr};
    std::errc ec{};
};

struct from_view_result {
    explicit constexpr operator bool() const {
        return ec == std::errc{};
    }
    std::string_view view{};
    std::errc ec{};
};

constexpr auto digit_to_number(char c, int base) noexcept -> int {
    constexpr auto base10c = static_cast<char>(base10);
    auto base_c = static_cast<char>(base);
    if ('0' <= c && c < '0' + base_c) {
        return c - '0';
    }
    if (base <= base10) {
        return -1;
    }
    if ('a' <= c && c <= ('a' + base_c - base10c)) {
        return c - 'a' + base10;
    }
    if ('A' <= c && c <= ('A' + base_c - base10c)) {
        return c - 'A' + base10;
    }
    return -1;
}

constexpr auto from_chars(const char *first, const char *last, std::integral auto &value, int base = base10)
    -> from_chars_result {
    if (first == last) {
        return {first, std::errc::invalid_argument};
    }
    if (*first == '-') {
        auto ret = from_chars(std::next(first), last, value, base);
        if (ret) {
            value = -value;
        }
        return ret;
    }
    auto digit = digit_to_number(*first, base);
    if (digit == -1) {
        return {first, std::errc::invalid_argument};
    }
    value = digit;

    for (++first; first != last && (digit = digit_to_number(*first, base)) != -1; ++first) {  // NOLINT
        // NOLINT-Reason: I kind of have to use pointer arithmetic here
        value = value * base + digit;
    }

    return {first};
};

constexpr auto from_chars(const char *first, const char *last, std::floating_point auto &value, int base = base10)
    -> from_chars_result {
    if (first == last) {
        return {first, std::errc::invalid_argument};
    }

    long long greater1{};
    const auto [ptr1, ec1] = from_chars(first, last, greater1, base);

    if (*ptr1 != '.') {
        if (ec1 == std::errc{}) {
            value = greater1;
        }
        return {ptr1, ec1};
    }

    long long smaller1{};
    const auto [ptr2, ec2] = from_chars(std::next(ptr1), last, smaller1, base);

    if (ec1 != std::errc{} && ec2 != std::errc{}) {
        return {ptr1, ec1};
    }

    const auto length = std::distance(ptr1, ptr2);
    value = greater1 + (static_cast<long double>(smaller1) / pow(static_cast<long double>(base), length));

    return {ptr2};
}

constexpr auto from_view(std::string_view sv, arithmetic auto &value, int base = base10) -> from_view_result {
    const auto [ptr, ec] = from_chars(begin(sv), end(sv), value, base);
    if (ec == std::errc{}) {
        return {std::string_view(ptr, end(sv)), ec};
    }
    return {sv, ec};
};
