#pragma once
#if __cpp_lib_constexpr_vector >= 201907L
#    include <vector>

template<class T>
using vector = std::vector<T>;
#else
#    include <cassert>
#    include <cstddef>
#    include <initializer_list>
#    include <iterator>
#    include <utility>

#    include "algorithms.hpp"

// This is much worse than std::vector. But I do not care, I just wanted something that would work at compile time.
template<class T>
struct vector : private std::allocator<T> {
    using iterator = T *;
    using const_iterator = const T *;

    using value_type = T;
    using size_type = size_t;
    using difference_type = std::ptrdiff_t;
    using reference = value_type &;
    using const_reference = const value_type &;
    using pointer = value_type *;
    using const_pointer = const value_type *;
    using reverse_iterator = std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;

    constexpr vector() = default;
    constexpr explicit vector(size_type s) : vector(s, T{}) {}

    constexpr vector(size_type s, const T &t) {
        resize_if_needed(s);
        m_end = m_begin + s;

        for (auto it = m_begin; it != m_end; ++it) {
            construct(it, t);
        }
    }

    template<class It>
    constexpr vector(It first, It last) requires std::forward_iterator<It> {
        resize_if_needed(rng::distance(first, last));
        rng::copy(first, last, std::back_inserter(*this));
    }

    constexpr vector(std::initializer_list<T> list) {
        resize_if_needed(list.size());
        rng::copy(list, std::back_inserter(*this));
    }

    constexpr vector(vector &&other) noexcept :
            std::allocator<T>(other),
            m_begin(std::exchange(other.m_begin, nullptr)),
            m_end(std::exchange(other.m_end, nullptr)),
            m_capacity(std::exchange(other.m_capacity, 0)) {}

    constexpr vector(const vector &other) : std::allocator<T>(other) {
        reserve(other.size());
        rng::copy(other, std::back_inserter(*this));
    }

    constexpr auto operator=(const vector &other) -> vector & {
        if (this == &other) {
            return *this;
        }
        auto will_be_destroyed(other);
        swap(will_be_destroyed);

        return *this;
    }

    constexpr auto operator=(vector &&other) noexcept -> vector & {
        if (this == &other) {
            return *this;
        }
        swap(other);
        return *this;
    }

    constexpr auto swap(vector &other) -> void {
        std::swap(m_begin, other.m_begin);
        std::swap(m_end, other.m_end);
        std::swap(m_capacity, other.m_capacity);
    }

    constexpr auto pop_back() -> void {
        destroy(--m_end);
    }

    constexpr auto clear() {
        for (auto it = rbegin(); it != rend(); ++it) {
            destroy(&*it);
        }
        m_end = m_begin;
    }

    constexpr auto reserve(size_type s) {
        resize_if_needed(s);
    }

    constexpr auto push_back(T &&t) -> void {
        resize_if_needed(size() + 1);
        construct(m_end, std::move(t));
        ++m_end;
    }

    constexpr auto push_back(const T &t) -> void {
        resize_if_needed(size() + 1);
        construct(m_end, t);
        ++m_end;
    }

    template<class... Args>
    constexpr auto emplace_back(Args &&...args) -> T &requires std::constructible_from<T, Args...> {
        resize_if_needed(size() + 1);
        construct(m_end, std::forward<Args>(args)...);
        ++m_end;
        return back();
    }

    constexpr auto resize(size_type s) -> void {
        if (size() < s) {
            resize(s, T{});
        } else {
            rng::repeat_n(size() - s, [&]() { pop_back(); });
        }
    }

    constexpr auto resize(size_type s, const value_type &val) -> void {
        if (size() < s) {
            resize_if_needed(s);
            rng::repeat_n(s - size(), [&]() { push_back(val); });
        } else {
            rng::repeat_n(size() - s, [&]() { pop_back(); });
        }
    }

    constexpr auto erase(const const_iterator first, const_iterator last) -> iterator {
        if (first == last) {
            return launder_const_iterator(last);
        }

        const auto size_before = size();
        const auto num_erased = rng::distance(first, last);
        const auto last_pos = rng::distance(m_begin, last);

        rng::move(last, m_end, launder_const_iterator(first));

        resize(size_before - num_erased);

        if (last_pos >= std::ssize(*this)) {
            return m_end;
        }
        return launder_const_iterator(last);
    }

    [[nodiscard]] constexpr auto empty() const -> bool {
        return m_begin == m_end;
    }

    [[nodiscard]] constexpr auto size() const -> size_type {
        return rng::distance(m_begin, m_end);
    }

    [[nodiscard]] constexpr auto capacity() const -> size_type {
        return m_capacity;
    }

    [[nodiscard]] constexpr auto front() const -> const_reference {
        return *begin();
    }
    constexpr auto front() -> reference {
        return *begin();
    }

    [[nodiscard]] constexpr auto back() const -> const_reference {
        return *std::prev(end());
    }
    constexpr auto back() -> reference {
        return *std::prev(end());
    }

    constexpr auto operator[](size_t index) const -> const_reference {
        return *(begin() + index);
    }
    constexpr auto operator[](size_t index) -> reference {
        return *(begin() + index);
    }

    [[nodiscard]] constexpr auto at(size_t index) const -> const_reference {
        assert(index < size());
        return (*this)[index];
    }
    constexpr auto at(size_t index) -> reference {
        assert(index < size());
        return (*this)[index];
    }

    [[nodiscard]] constexpr auto cbegin() const -> const_iterator {
        return (m_begin);
    }
    [[nodiscard]] constexpr auto begin() const -> const_iterator {
        return (m_begin);
    }
    constexpr auto begin() -> iterator {
        return (m_begin);
    }

    [[nodiscard]] constexpr auto cend() const -> const_iterator {
        return (m_end);
    }
    [[nodiscard]] constexpr auto end() const -> const_iterator {
        return (m_end);
    }
    constexpr auto end() -> iterator {
        return (m_end);
    }

    [[nodiscard]] constexpr auto crbegin() const -> const_reverse_iterator {
        return std::make_reverse_iterator(m_end);
    }
    [[nodiscard]] constexpr auto rbegin() const -> const_reverse_iterator {
        return std::make_reverse_iterator(m_end);
    }
    constexpr auto rbegin() -> reverse_iterator {
        return std::make_reverse_iterator(m_end);
    }

    [[nodiscard]] constexpr auto crend() const -> const_reverse_iterator {
        return std::make_reverse_iterator(m_begin);
    }
    [[nodiscard]] constexpr auto rend() const -> const_reverse_iterator {
        return std::make_reverse_iterator(m_begin);
    }
    constexpr auto rend() -> reverse_iterator {
        return std::make_reverse_iterator(m_begin);
    }

    constexpr ~vector() {
        clear();
        if (m_begin != nullptr) {
            at_t::deallocate(*this, m_begin, capacity());
        }
    }

  private:
    using at_t = std::allocator_traits<std::allocator<T>>;

    constexpr auto increase_end(size_type increment = 1) {
        m_end = end() + sizeof(T) * increment;  // NOLINT
    }

    template<class... Args>
    constexpr auto construct(T *ptr, Args &&...args) {
        at_t::construct(*this, ptr, std::forward<Args>(args)...);
    }

    constexpr auto destroy(T *ptr) {
        at_t::destroy(*this, ptr);
    }

    constexpr auto launder_const_iterator(const_iterator it) -> iterator {
        return m_begin + rng::distance(m_begin, it);
    }

    constexpr auto resize_if_needed(size_type new_size) -> void {
        if (new_size < m_capacity) {
            return;
        }

        const auto old_size = size();
        const auto new_capacity = std::bit_ceil(new_size + 1);
        auto *const new_begin = at_t::allocate(*this, new_capacity);

        auto *it = new_begin;
        if (m_begin != nullptr) {
            for (auto i = m_begin; i < m_end; ++it, ++i) {
                construct(it, std::move(*i));
            }

            clear();
            at_t::deallocate(*this, m_begin, m_capacity);
        }

        m_begin = new_begin;
        m_end = new_begin + old_size;
        m_capacity = new_capacity;
    }


    T *m_begin{nullptr};
    T *m_end{nullptr};
    size_t m_capacity{};
};
static_assert(rng::range<vector<int>>);

template<class T, class Pred>
constexpr auto erase_if(vector<T> &v, Pred &&pred) {
    const auto subrange = rng::remove_if(v, pred);
    const auto r = std::size(subrange);
    v.erase(std::begin(subrange), v.end());
    return r;
}
#endif
