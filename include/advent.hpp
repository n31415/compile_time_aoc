#pragma once

#include "algorithms.hpp"
#include "constexpr_char_conv.hpp"
#include "constexpr_maths.hpp"
#include "defs.hpp"
#include "field.hpp"
#include "flatmap.hpp"
#include "io.hpp"
#include "non_constexpr.hpp"
#include "utility.hpp"

using namespace std::literals;
