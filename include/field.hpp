#pragma once

#include "algorithms.hpp"
#include <algorithm>
#include <cassert>
#include <cctype>
#include <functional>
#include <iterator>
#include <numeric>

#include "io.hpp"
#include "utility.hpp"

template<class T = size_t, size_t Dimension = 2>
struct Field {
    static constexpr auto dimension = Dimension;
    using point_t = PointNd<dimension>;
    using value_t = T;

    [[nodiscard]] constexpr auto point_from_index(size_t index) const -> point_t {
        auto index_l = static_cast<long>(index);
        point_t ret;
        for (auto i = dimension; i-- > 0;) {
            std::tie(index_l, ret.at(i)) = div_rest(index_l, dimensions.at(i));
        }
        return ret;
    }

    [[nodiscard]] constexpr auto index_from_point(const point_t &p) const -> size_t {
        auto ret = size_t{};
        for (auto i = dimension; i-- > 0;) {
            ret = ret * dimensions.at(i) + p.at(i);
        }
        return ret;
    }

    template<class ValueType>
    [[nodiscard]] constexpr auto create_nd(ValueType v = {}) const -> vector<ValueType> {
        return vector<ValueType>(rng::accumulate(dimensions.coords, {}, std::multiplies<>{}), v);
    }

    template<class ValueType>
    [[nodiscard]] constexpr auto create_2d(ValueType &&v = {}) const -> vector<ValueType> requires(dimension == 2) {
        return create_nd(std::forward<ValueType>(v));
    }

    [[nodiscard]] constexpr auto check_internal(const point_t &p) const -> bool {
        return rng::all_of(std::views::iota(0UL, dimension),
                           [&](size_t i) { return 0 <= p.at(i) && p.at(i) <= dimensions.at(i); });
    }

    template<class Vec>
    [[nodiscard]] constexpr auto twod_access(const point_t &p, Vec &v) const
        -> copy_const_t<Vec, typename Vec::value_type> & {
        return v.at(index_from_point(p));
    }

    [[nodiscard]] constexpr auto get_content(const point_t &p) const -> value_t {
        return twod_access(p, content);
    }

    [[nodiscard]] constexpr auto get_content(const point_t &p) -> value_t & {
        return twod_access(p, content);
    }

    template<class Func>
    constexpr auto for_each_neighbor(const point_t &p, Func &&f) const -> void {
        for (const auto inc: {-1, 1}) {
            const auto px = point_t{p.x() + inc, p.y()};
            const auto py = point_t{p.x(), p.y() + inc};
            if (check_internal(px)) {
                f(px);
            }
            if (check_internal(py)) {
                f(py);
            }
        }
    }

    template<class Func>
    constexpr auto for_each_diagonal(const point_t &p, Func &&f) const -> void {
        for (const auto dx: {-1, 1}) {
            for (const auto dy: {-1, 1}) {
                const auto pd = point_t{p.x() + dx, p.y() + dy};
                if (check_internal(pd)) {
                    f(pd);
                }
            }
        }
    }


    template<class Func>
    constexpr auto for_all_points(Func &&f) const -> void {
        rng::for_each(std::views::iota(0UL, content.size()),
                      [&](size_t i) { call_longest(f, content[i], i, point_from_index(i)); });
    }

    [[nodiscard]] constexpr auto width() const -> long {
        return dimensions.x();
    }
    constexpr auto width() -> long & {
        return dimensions.x();
    }
    [[nodiscard]] constexpr auto height() const -> long {
        return dimensions.y();
    }
    constexpr auto height() -> long & {
        return dimensions.y();
    }

    constexpr explicit Field(const point_t &size, vector<value_t> content) :
            dimensions(size), content(std::move(content)) {}
    constexpr explicit Field(const point_t &size) : Field(size, vector<value_t>(size.x() * size.y())) {}
    constexpr Field() = default;

    point_t dimensions{};
    vector<value_t> content{};
};

template<class T>
static constexpr auto read2d(std::string_view &sv) -> Field<T, 2UL> {
    Field<T, 2UL> field;
    remove_ws(sv);
    field.width() = rng::find_if_pos(sv, &is_ws);
    while (!sv.empty()) {
        const auto c = sv.front();
        sv.remove_prefix(1);
        if (!is_ws(c)) {
            if constexpr (std::is_same_v<T, char>) {
                field.content.push_back(c);
            } else {
                field.content.push_back(c - '0');
            }
        }
    }

    assert(field.width() > 0);
    field.height() = ssize(field.content) / field.width();

    return field;
}
