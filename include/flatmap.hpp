#pragma once
#include <array>
#include <cstddef>

#include "algorithms.hpp"
#include "constexpr_vector.hpp"
#include <utility>

template<class Key, class Value, size_t N>
using flatmap_t = std::array<std::pair<Key, Value>, N>;

template<class Key, class Value>
using dynamic_flatmap_t = vector<std::pair<Key, Value>>;

template<class Key, class Value>
constexpr auto key_proj(const std::pair<Key, Value> &p) -> const Key & {
    return p.first;
}

template<class Key, class Value>
constexpr auto val_proj(const std::pair<Key, Value> &p) -> const Value & {
    return p.second;
}

template<class FlatMap, class Pair = rng::range_value_t<FlatMap>, class Key = std::tuple_element_t<0, Pair>,
         class Value = std::tuple_element_t<1, Pair>>
constexpr auto flat_at(const FlatMap &map, const Key &key) -> const Value & {
    auto ret = rng::find(map, key, &key_proj<Key, Value>);
    return ret->second;
}
