#pragma once

#ifdef ALL_THE_THINGS
#    define MAYBE_CONSTEXPR constexpr
#else
#    define MAYBE_CONSTEXPR
#endif
