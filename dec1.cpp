#include "include/advent.hpp"
#include "inputs/dec1.hpp"

constexpr auto num_increases(const vector<int> &heights) {
    return std::inner_product(
        begin(heights), std::prev(end(heights)), std::next(begin(heights)), 0, std::plus<>{}, std::less<>{});
}

constexpr auto part1(std::string_view inp) {
    auto heights = read_vector<int>(inp, base10);
    return num_increases(heights);
}

constexpr auto part2(std::string_view inp) {
    constexpr auto window_width = 3;
    auto heights = read_vector<int>(inp, base10);
    vector<int> sliding_window(heights.size() + 1 - window_width);
    for (auto i = 0; i < window_width; ++i) {
        std::transform(begin(sliding_window),
                       end(sliding_window),
                       std::next(begin(heights), static_cast<long>(i)),
                       begin(sliding_window),
                       std::plus<>{});
    }

    return num_increases(sliding_window);
}

#include "include/main.hpp"
