#include "include/advent.hpp"
#include "inputs/dec14.hpp"

constexpr auto num_chars = size_t{256};

struct Rule {
    std::array<char, 2> educts{};
    char product{};
};

struct MappedRuleset {
    [[nodiscard]] constexpr auto id_to_pair(size_t id) const -> std::array<int, 2> {
        return {static_cast<int>(id / num_chars), static_cast<int>(id % num_chars)};
    }
    [[nodiscard]] constexpr auto pair_to_id(int one, int two) const -> size_t {
        auto pos = one * num_chars + two;
        return pos;
    }
    [[nodiscard]] constexpr auto pair_to_id(const std::array<int, 2> &pair) const -> size_t {
        return pair_to_id(pair.front(), pair.back());
    }

    [[nodiscard]] constexpr auto rewrites(size_t index) const -> std::array<size_t, 2> {
        const auto inserted = replacements[index];
        const auto [front, back] = id_to_pair(index);
        return {pair_to_id({front, inserted}), pair_to_id({inserted, back})};
    }

    size_t num_chars{};
    vector<char> pattern{};
    vector<int> replacements{};
};

constexpr auto read_rule(std::string_view &in) -> Rule {
    Rule ret;
    remove_ws(in);
    for (auto &e: ret.educts) {
        e = get_char(in);
    }

    getline(in, '>');
    remove_ws(in);
    ret.product = get_char(in);

    return ret;
}

constexpr auto to_numbers(const std::string_view &pattern, const vector<Rule> &rules) -> MappedRuleset {
    vector<char> all_chars;
    all_chars.reserve(size(pattern) + 2 * size(rules));
    auto inserter = std::back_inserter(all_chars);

    rng::copy(pattern, inserter);
    rng::transform(rules, inserter, [&](const auto &rule) { return rule.educts.front(); });
    rng::transform(rules, inserter, [&](const auto &rule) { return rule.educts.back(); });

    rng::sort(all_chars);
    const auto [new_end, old_end] = rng::unique(all_chars);
    all_chars.erase(new_end, old_end);


    const auto num_chars = all_chars.size();

    MappedRuleset ret{num_chars};
    ret.pattern.resize(size(pattern));

    rng::transform(pattern, begin(ret.pattern), [&](char c) { return rng::find_pos(all_chars, c); });

    ret.replacements.resize(num_chars * num_chars);
    for (auto rule: rules) {
        rng::transform(rule.educts, begin(rule.educts), [&](char c) { return rng::find_pos(all_chars, c); });
        const auto index = ret.pair_to_id(rule.educts.front(), rule.educts.back());
        ret.replacements[index] = static_cast<int>(rng::find_pos(all_chars, rule.product));
    }

    return ret;
}

constexpr auto rewrite(const MappedRuleset &mapped, vector<size_t> occurences) -> vector<size_t> {
    vector<size_t> ret(occurences.size(), 0);

    for (auto i = size_t{}; i < occurences.size(); ++i) {
        const auto occured = occurences[i];
        const auto [a, b] = mapped.rewrites(i);
        ret[a] += occured;
        ret[b] += occured;
    }

    return ret;
}

constexpr auto read_rules(std::string_view &in) -> vector<Rule> {
    vector<Rule> ret;
    for (Rule buf = read_rule(in); buf.product != 0; buf = read_rule(in)) {
        ret.emplace_back(buf);
    }

    return ret;
}

constexpr auto pair_occurences_to_single(const MappedRuleset &mapped, const vector<size_t> &pairs) {
    vector<size_t> ret(mapped.num_chars, 0);
    for (auto i = size_t{}; i < size(pairs); ++i) {
        for (const auto singles = mapped.id_to_pair(i); const auto s: singles) {
            ret[s] += pairs[i];
        }
    }
    return ret;
}

constexpr auto worker(std::string_view inp, size_t num_rounds) -> size_t {
    const auto pattern = read_one(inp);
    const auto rules = read_rules(inp);

    const auto mapped = to_numbers(pattern, rules);
    vector<size_t> occurences(mapped.replacements.size(), 0);
    // One could use std::adjacent_difference for this if it would actually compute the adjacent difference.
    for (auto i = size_t{}; i + 1 < mapped.pattern.size(); ++i) {
        const auto index = mapped.pair_to_id({mapped.pattern[i], mapped.pattern[i + 1]});
        ++occurences[index];
    }

    rng::repeat_n(num_rounds, [&]() { occurences = rewrite(mapped, std::move(occurences)); });
    const auto frequency = pair_occurences_to_single(mapped, occurences);
    const auto [mi, ma] = rng::minmax_element(frequency);

    return ((*ma + 1) / 2 - (*mi + 1) / 2);
}

constexpr auto part1(std::string_view inp) -> size_t {
    return worker(inp, 10);
}

constexpr auto part2(std::string_view inp) -> size_t {
    return worker(inp, 40);
}

#include "include/main.hpp"
