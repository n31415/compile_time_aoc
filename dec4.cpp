#include "include/advent.hpp"
#include "inputs/dec4.hpp"

constexpr auto BoardSize = 5UL;

constexpr auto read_called(std::string_view &inp) -> vector<int> {
    vector<int> ret;
    remove_ws(inp);
    std::string_view buff = getline(inp);

    while (auto num = read_one<int>(buff)) {
        ret.push_back(*num);
        if (!buff.empty()) {
            buff.remove_prefix(1);
        }
    }
    return ret;
}

struct bingo_board {
    [[nodiscard]] constexpr auto check_row(size_t index) const -> bool {
        const auto *start = std::begin(checked) + BoardSize * index;
        return rng::all_of(std::views::counted(start, BoardSize), std::identity{});
    }

    [[nodiscard]] constexpr auto check_column(size_t index) const -> bool {
        for (auto i = index; i < BoardSize * BoardSize; i += BoardSize) {
            if (!checked.at(i)) {
                return false;
            }
        }
        return true;
    }

    [[nodiscard]] constexpr auto sum() const -> size_t {
        return std::inner_product(
            begin(board), end(board), begin(checked), 0UL, std::plus<>{}, [](auto num, auto checked) {
                return !checked ? num : 0;
            });
    }

    constexpr auto call_num(size_t num) -> bool {
        if (won) {
            return true;
        }

        for (auto r = 0UL; r < BoardSize; ++r) {
            for (auto c = 0UL; c < BoardSize; ++c) {
                const auto index = r * BoardSize + c;
                if (board[index] == num) {
                    checked[index] = true;
                    if (check_row(r) || check_column(c)) {
                        won = true;
                        return true;
                    }
                }
            }
        }
        return false;
    }

    static constexpr auto read(std::string_view &inp) -> std::optional<bingo_board> {
        bingo_board ret;
        for (auto &field: ret.board) {
            const auto buf = read_one<size_t>(inp);
            if (!buf) {
                return {};
            }
            field = *buf;
        }
        return ret;
    }

    std::array<size_t, BoardSize * BoardSize> board{0};
    std::array<bool, BoardSize * BoardSize> checked{false};
    bool won = false;
};

constexpr auto read_boards(std::string_view &inp) -> vector<bingo_board> {
    vector<bingo_board> boards;
    while (auto bingo = bingo_board::read(inp)) {
        boards.push_back(*bingo);
    }
    return boards;
}

constexpr auto part1(std::string_view inp) -> size_t {
    auto called_nums = read_called(inp);
    auto boards = read_boards(inp);
    for (const auto num: called_nums) {
        auto *const winner = rng::find_if(boards, [&](auto &board) { return board.call_num(num); });
        if (winner != end(boards)) {
            return winner->sum() * num;
        }
    }
    assert(false);
}

constexpr auto part2(std::string_view inp) -> size_t {
    const auto called_nums = read_called(inp);
    auto boards = read_boards(inp);

    for (const auto num: called_nums) {
        auto not_won = boards | std::views::filter(std::not_fn(&bingo_board::won));
        auto first = begin(not_won);
        for (auto &board: not_won) {
            board.call_num(num);
        }
        if (rng::all_of(boards, &bingo_board::won)) {
            return first->sum() * num;
        }
    }
    assert(false);
}

#include "include/main.hpp"
