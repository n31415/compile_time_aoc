#include "include/advent.hpp"
#include "include/constexpr_char_conv.hpp"
#include "include/constexpr_maths.hpp"
#include "include/io.hpp"
#include "inputs/dec17.hpp"

struct TargetArea {
    constexpr TargetArea(long x1, long x2, long y1, long y2) :
            min_pos{std::min(x1, x2), std::min(y1, y2)}, max_pos{std::max(x1, x2), std::max(y1, y2)} {}

    [[nodiscard]] constexpr auto in_area(Point2D p) const -> bool {
        return (min_pos.x() <= p.x()) && (min_pos.y() <= p.y()) && (max_pos.x() >= p.x()) && (max_pos.y() >= p.y());
    }

    Point2D min_pos;
    Point2D max_pos;
};

constexpr auto read_target_area(std::string_view input) -> TargetArea {
    skip_to_integral(input);
    const auto x1 = *read_one<long>(input);
    skip_to_integral(input);
    const auto x2 = *read_one<long>(input);
    skip_to_integral(input);
    const auto y1 = *read_one<long>(input);
    skip_to_integral(input);
    const auto y2 = *read_one<long>(input);

    return {x1, x2, y1, y2};
}

constexpr auto gauss(long n) -> long {
    return n * (n + 1) / 2;
}

struct Probe {
    constexpr explicit Probe(Point2D v) : velocity{v}, final_x{gauss(v.x())} {}

    constexpr auto tick() -> Point2D {
        for_each_index<1>([&]<size_t I>() { get<I>(position) += get<I>(velocity); });

        velocity.x() -= signum(velocity.x());
        velocity.y() -= 1;

        return position;
    }

    [[nodiscard]] constexpr auto final_probe() const -> Probe {
        const auto final_y = velocity.x() * velocity.y() - final_x;
        const auto final_vy = velocity.y() - velocity.x();
        auto p = *this;
        p.position = {final_x, final_y};
        p.velocity = {0, final_vy};

        return p;
    }

    Point2D velocity;
    long final_x;
    Point2D position{};
};

constexpr auto can_reach(const Probe &pro, const TargetArea &area) -> bool {
    if (pro.velocity.y() <= 0 && pro.position.y() < area.min_pos.y()) {
        return false;
    }
    if (pro.position.x() > area.max_pos.x()) {
        return false;
    }
    if (pro.position.x() + gauss(pro.velocity.x()) < area.min_pos.x()) {
        return false;
    }

    return true;
}

constexpr auto test_probe(Probe pro, const TargetArea &area) -> std::pair<long, long> {
    for (long ticks = 0, max_y = 0; can_reach(pro, area); pro.tick(), ++ticks) {
        update_max(max_y, pro.position.y());
        if (area.in_area(pro.position)) {
            return {ticks, max_y};
        }
    }
    return {-1, -1};
}

constexpr auto x_range(const TargetArea &area) {
    return rng::iota_view{0, area.max_pos.x() + 1};
}

constexpr auto y_range(const TargetArea &area) {
    return rng::iota_view{area.min_pos.y(), -area.min_pos.y() + 1};
}

template<class Func>
constexpr auto test_all(const TargetArea &area, Func &&f) {
    for (auto x: x_range(area)) {
        for (auto y: y_range(area)) {
            Probe p{{x, y}};
            auto [ticks, max_y] = test_probe(p, area);
            if (ticks != -1) {
                std::invoke(f, max_y);
            }
        }
    }
}

constexpr auto part1(std::string_view inp) -> long {
    const auto area = read_target_area(inp);
    long highest = 0;
    auto func = [&](const auto max_y) { update_max(highest, max_y); };
    test_all(area, func);

    return highest;
}

constexpr auto part2(std::string_view inp) -> size_t {
    const auto area = read_target_area(inp);
    auto count = size_t{};
    auto func = [&](const auto &.../*unused*/) { ++count; };

    test_all(area, func);

    return count;
}

#include "include/main.hpp"
